<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Registrar Usuario</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords"
        content="spa" />
    <!-- //Meta tag Keywords -->
	 <link rel="stylesheet" href="css/register.css" type="text/css" media="all" /><!-- Style-CSS -->
</head>
<body>
   <!-- /form-26-section -->
   <section class="form-26-1">
         <div class="form-26-mian">
				<div class="layer">
			<div class="wrapper">
			<div class="form-inner-cont">
					<div class="forms-26-info">
						<h2>Registro</h2>
                        <p>Registrate para ingresar al sistema de gestión de spa.</p>
                    </div>
					<div class="form-right-inf"> 
							<form action="guardarUsuario.php" method="post" class="signin-form">	
							 <div class="forms-gds">
								 <div  class="form-input">
									<input type="text" name="nombre" placeholder="Nombre" required />
								</div>
								<div  class="form-input">
										<input type="text" name="apellido" placeholder="Apellido" required />
									</div>
								<div  class="form-input">
									<input type="email" name="correo" placeholder="Correo" required />
								</div>
								<div  class="form-input">
									<input type="password" name="contrasena1" placeholder="Contraseña" required />
								</div>
								<div  class="form-input">
									<input type="password" name="contrasena2" placeholder="Confirmar Contraseña" required />
								</div>
								<div  class="form-input"><button class="btn">Registrar</button></div>
							</div>
							<h6 class="already">Ya tienes cuenta? <a href="login.php"><span>Iniciar sesión<span></span></span></a></h6>
							<h6 class="already">Regrsar al inicio sin iniciar sesión <a href="index.php"><span>Inicio<span></span></span></a></h6>
						</form>
						
                    </div>
					<div class="copyright text-center">
                    <p>© 2019 Merged Forms. All rights reserved | Design by <a href="http://w3layouts.com/"
                            target="_blank">W3Layouts</a></p>
                </div>
                </div>
			</div>
		</div>
		    </div>
		</section>
      <!-- //form-26-section -->
</body>
</html>