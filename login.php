<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Login</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords"
        content="Merged Forms Page Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <!-- //Meta tag Keywords -->
     <link rel="stylesheet" href="css/login.css" type="text/css" media="all" /><!-- Style-CSS -->
</head>
<body>
   <!-- /form-26-section -->
   <section class="form-26">
         <div class="form-26-mian">
				<div class="layer">
			<div class="wrapper">
			<div class="form-inner-cont">
					<div class="forms-26-info">
						 <h2>Iniciar Sesión</h2>
                        <p>SPA</p>
                    </div>
					<div class="form-right-inf"> 
							<form action="validarLogin.php" method="post" class="signin-form">	
							 <div class="forms-gds">
								<div  class="form-input">
									<input type="email" name="correo" placeholder="Correo" required />
								</div>
								<div  class="form-input">
									<input type="password" name="contrasena" placeholder="Contraseña" required />
								</div>
								<div  class="form-input"><button class="btn">Iniciar Sesión</button></div>
							</div>
							<h6 class="already"> Ya tienes cuenta? <a href="register.php"><span>Registrate aquí<span></span></span></a></h6>
							<h6 class="already">Regrsar al inicio <a href="index.php"><span>Inicio<span></span></span></a></h6>
						</form>
						
                    </div>
				<div class="copyright text-center">
                    <p>© 2020</p>
                </div>
                </div>
			
			</div>
		</div>
		    </div>
		</section>
      <!-- //form-26-section -->
</body>
</html>